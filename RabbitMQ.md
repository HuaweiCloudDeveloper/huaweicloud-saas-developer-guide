# RabbitMQ<a name="ZH-CN_TOPIC_0000001477290193"></a>

RabbitMQ作为消息中间件，在SaaS系统中使用，主要需要注意的是如何实现租户上下文的传递，其次是如何实现租户数据的隔离，一般来说这两点分别对应着数据级隔离和应用级隔离，通常实现其一即可。

## 租户上下文的传递<a name="section640217111924"></a>

-   消息头包含租户上下文

    RabbitMq的消息结构中包含消息头，相当于http头，可以携带信息体以外的上下文信息，租户信息作为关键的上下文也可以放到消息头中传递。

    -   消息生产者

        ```
        static class TenantMessageProcessor implements MessagePostProcessor {
            @Override
            public Message postProcessMessage(Messagemessage) throws AmqpException {
                StringtenantDomain = TenantContext.getDomain();
                log.info("{} send message with rabbit", tenantDomain);
                // 消息发送前保存租户标识
                Optional.ofNullable(tenantDomain)
                    .ifPresent(domain ->message.getMessageProperties().setHeader(Constants.TENANT_DOMAIN, tenantDomain));
                return message;
            }
        }
        ```

        MessagePostProcessor （org.springframework.batch.sample.rabbitmq.processor）是Spring框架中对RaabbitMQ消息的处理器接口，可以在消息中增加或修改内容。SaaS系统中，我们可以使用利用这个处理器在RabitMQ消息头增加租户标识。

        在SpringBoot里，可以在包含@Configuration的类中注入一个RabbitTemplate 的 Bean，RabbitTemplate是一个消息模板，它帮助我们简化消息的处理。

        ```
        @Bean("tenantRouteRabbitTemplate")
        @ConditionalOnProperty(prefix = "tenant.beans", name = "rabbitmq-route-enabled", havingValue = "true"
        )
        public RabbitTemplate rabbitTemplate
        (CachingConnectionFactory factory) {
            RabbitTemplate rabbitTemplate = new RabbitTemplate(factory);
            // 把租户处理器加入到post前置处理器集合中
            rabbitTemplate.setBeforePublishPostProcessors(new TenantMessageProcessor());
         return rabbitTemplate;
        ```

        -   @ConditionalOnProperty这段的意思是当配置文件中tenant.beans.rabbitmq-route-enabled为true的时候，我们才注入这个Bean.
        -   @Bean的名称这里设置为“tenantRouteRabbitTemplate”，这个命名信息不加的话就默认只有这个Redistemplate，在Springboot里我们可以设置多个相同类型的Bean，在程序中可能有些共有消息我们不需要带租户标识，或者需要携带其他的信息，那就可以另外命名一个Bean来注入到SpringBoot中。
        -   “CachingConnectionFactory ”是消息连接工厂，在SpringBoot里按照约定配置相关内容的话，这个工厂是自动注入的。

    -   消息消费者

        消费者接收消息也需要在Springboot中注入一个消息监听工厂，如果我们需要接收携带租户标识的上下文，可以在这个工厂中放入一个消息处理器去单独接收租户标识，把这个标识放到微服务的请求上下文中。

        ```
        @Bean("rabbitListenerContainerFactory")
        // 默认的监听模式为simple
        @ConditionalOnProperty(prefix = "spring.rabbitmq.listener", name = "type", havingValue = "simple",
                matchIfMissing = true)
        public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(
                SimpleRabbitListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
            SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
            factory.setAfterReceivePostProcessors(message -> {
                Optional.ofNullable(message).ifPresent(msg -> {
                    String tenantDomain = msg.getMessageProperties().getHeader(Constants.TENANT_DOMAIN);
                    //消息头中若包含租户标识，则将它放入会话上下文
                    Optional.ofNullable(tenantDomain).ifPresent(td -> {
                        TenantContext.setDomain(td, true);
                    });
                });
                return message;
            });
            configurer.configure(factory, connectionFactory);
            return factory;
        }
        ```

        -   rabbitMq的监听模式有很多种，不同模式使用的监听工厂也是不一样的，上面代码使用simple模式的监听作为示范。
        -   TenantContext是我们自定义的一个会话上下文环境，使用了一个HystrixRequestVariableDefault作为会话中的对象容器，这个东西其实是对ThreadLocal的一个包装，使用这个包装后的容器可以适应多线程等更多的环境。

            ```
            public class TenantContext {
                private static final HystrixRequestVariableDefault<Map<String, Object>> GLOBAL_CACHE
                    = new HystrixRequestVariableDefault<>();
            ```

        经过这样的处理，租户上下文就可以在发送消息的同时在两个微服务中传递。



-   消息体包含租户上下文

    这种方式比较容易理解，在消息体内存入租户id，建议使用一个包装类来包装所有需要租户上下文的消息，整个传递过程和消息头的处理差不多，


## 租户数据的隔离<a name="section152251745630"></a>

在消息中间件中隔离租户数据一般是在应用级租户隔离的方案中使用。

RabbitMQ通道本身可根据RabbitMQ实现不同级别的隔离。物理级RabbitMQ隔离，不同租户使用不同的RabbitMQ服务；virturalhost隔离，这是RabbitMQ最上层的逻辑隔离；交换机隔离；队列隔离

-   物理级隔离和virtualhost隔离，可在RabbitMQ的连接中配置。
-   交换机和队列隔离

    RabbitMQ使用AMQP协议，有交换机和队列的概念，以快递收发比喻，交换机相当于快递公司，队列相当于投递员。一个交换机可以对应多个队列。

    了解一下交换机类型：

    -   直连交换机：队列名称和Routingkey一致，相当于邮件与投递员一对一绑定
    -   主题交换机：按照匹配模式将消息推送给一个或多个队列，相当于订阅
    -   头交换机：使用消息头代替Routingkey，工作模式和直连交换机一样
    -   散型交换机：将消息派给绑定到它上的所有队列，所有队列收到同一份消息，一般用来做广告

    我们了解交换机几种类型和队列的关系后，可以整理出这几种隔离方案：

    -   交换机隔离：队列是绑定交换机的，所以交换机做租户隔离的话队列就自然隔离。
    -   使用Routingkey做租户隔离：可使用直连交换机或者主题交换机
    -   使用消息头做租户隔离：使用头交换机


其中使用主题交换机来实现消息路由是最灵活的，可优先选用。这几种方案在技术和性能上影响不大，可在根据业务选择适合的方案优化调度。

