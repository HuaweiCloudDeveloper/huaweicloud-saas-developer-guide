# VPC 隔离模式<a name="ZH-CN_TOPIC_0000001271256364"></a>

[虚拟私有云（VPC）](https://support.huaweicloud.com/function-vpc/index.html)与region、可用区AZ相比，更多的是为云服务器、云容器、云数据库等云上资源构建隔离、私密的虚拟网络环境，提供的是一种逻辑隔离策略，而不是实际的物理隔离模式。默认情况下，不同VPC内的资源相互隔离，不能互相访问。在SaaS应用上云时，根据客户需求和业务量规模，将多个租户或者单个租户应用部署到同一个VPC中。在下图示例中，我们将应用层和数据层部署在不同VPC中（VPC1与VPC3、VPC2与VPC3），不同租户间的应用层部署在不同VPC（VPC1、VPC2）；为实现数据库资源共享，我们通过华为云上VPC-Peering服务，实现同region的VPC打通，实现应用层与数据层内网访问需求，示例中展示了共享资源池模式下，通过VPC模式进行隔离，有以下优势：

-   将不同租户应用层之间采用VPC隔离，不同租户间访问性能互不干扰。
-   将应用层与数据层通过VPC隔离，保证数据层的数据安全。

但不足在于，采用VPC进行租户隔离时，需要考虑账号VPC配额创建限制，具体操作请参考[申请配额](https://support.huaweicloud.com/usermanual-iaas/zh-cn_topic_0040259342.html)；另外，应用部署时，需要考虑网络关系，稍微复杂。

![](figures/zh-cn_image_0000001322392457.png)

