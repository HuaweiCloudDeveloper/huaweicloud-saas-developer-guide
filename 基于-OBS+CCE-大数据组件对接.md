# 基于“OBS+CCE”大数据组件对接<a name="ZH-CN_TOPIC_0000001270776608"></a>

## 前提条件<a name="zh-cn_topic_0000001227593608_section20541174354314"></a>

-   已安装Hadoop。
-   已[创建CCE集群](https://support.huaweicloud.com/intl/zh-cn/usermanual-cce/cce_01_0028.html)，且集群下有可用节点，集群内节点已绑定弹性IP，且配置了kubectl命令行工具。

## 组件搭建<a name="zh-cn_topic_0000001227593608_section77827716443"></a>

-   **[Hadoop对接OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1507.html)**

    OBS服务实现了Hadoop的HDFS协议，在大数据场景中可以替代Hadoop系统中的HDFS服务，为大数据计算提供“数据湖”存储。


-   **[hive对接OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1508.html)**

    Hive可以对存储在分布式存储中的大规模数据进行数据提取、转化和加载，它提供了丰富的SQL查询方式来进行数据分析。


-   **[Flink对接OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1516.html)**

    Flink定义了文件系统抽象，OBS服务实现了Flink的文件系统抽象，使得OBS可以作为flink StateBackend和数据读写的载体。


-   **[CCE部署使用Flink](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_0121.html)**

    在华为云CCE集群中部署flink集群的流程说明。


-   **[ClickHouse on CCE部署指南](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_0128.html)**

    在CCE集群中安装部署clickhouse-operator并举例创建clickhouse集群资源。clickhouse-operator安装要求k8s版本为1.15.11+，本文使用1.19。


