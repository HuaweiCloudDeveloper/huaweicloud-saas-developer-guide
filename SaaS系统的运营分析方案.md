# SaaS系统的运营分析方案<a name="ZH-CN_TOPIC_0000001416368846"></a>

SaaS系统与独立项目有一点重要区别，独立项目通常是针对某一客户的，客户的需求可以通过交流去获取，但SaaS系统面对的是众多的客户，不可能满足所有客户的需求，SaaS提供商才是系统的决策者，所以对SaaS提供商而言，分析租户及租户下用户的行为，对于系统优化、系统营销策略以至于租户的生命周期都有极大的影响。

## 互动节点<a name="section1039618411138"></a>

以一个潜在租户的行为为例，他通过相关的宣传连接到SaaS提供商的官网，查看系统介绍，填入企业信息申请产品试用，试用时访问具体的功能模块，这些行为节点都可以进行具体的分析。

## 埋点方案<a name="section84636016139"></a>

## 分析方案<a name="section7970171951420"></a>

分析需求是从业务需求中获取

例如从宣传连接的跳转中可以做出渠道分析

从系统介绍的浏览时长可以做出用户的兴趣点分析

从各个阶段进入下一阶段的比例可以找到客户留存率

从用户提供的信息可以做用户的企业画像

从功能模块的试用时间、操作数量可以做出关注度分析

## 分析模型<a name="section1934433965715"></a>

数据分析模型有漏斗、留存、用户路径、点击热点等

数据架构

## 分析案例<a name="section638442392712"></a>

SaaS应用运营有两个重要的指标，一个是客户获取，一个是续费

-   客户获取是互联网应用的重要指标，通过一系列的宣传活动和系统特性去吸引用户使用，适合使用漏斗函数对这个过程进行分析。
-   SaaS系统最重要的是续费，统计用户的续费率是对SaaS项目最重要的反馈，留存函数适用于续费率的统计。

## 访问所有租户的数据<a name="section16542614175614"></a>

运营分析可使用日志数据和业务数据作为数据源

对SaaS系统的业务数据进行分析，系统需要有一个超级用户能够获取所有用户的数据。

多数据库弹性查询：

数据仓库：

数据湖：

