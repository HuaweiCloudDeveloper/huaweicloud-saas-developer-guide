# namespace隔离模式<a name="ZH-CN_TOPIC_0000001270936476"></a>

![](figures/zh-cn_image_0000001322072333.png)

在共享资源池模式下，选择Kubernetes集群为SaaS应用开发者提供的namespace隔离方式是非常明智的，华为云容器引擎CCE在namespace粒度提供了网络隔离、资源配额限制以及RBAC权限管理策略等租户管控策略。

-   [网络隔离](https://support.huaweicloud.com/usermanual-cce/cce_01_0286.html)，CCE基于Kubernetes的网络策略功能进行了加强，通过配置网络策略，允许在同个集群内实现网络的隔离，也就是可以在某些实例（Pod）之间架起防火墙。例如命名空间default，网络隔离的默认状态为“隔离状态未开启”，表示“当前集群下的所有工作负载”都可以访问“命名空间default下的工作负载”。如果在namespace中开启设置“kind: NetworkPolicy”策略并禁止其他命名空间pod访问，当然，也可以在[CCE控制台](https://console.huaweicloud.com/cce2.0/?region=cn-north-4#/app/resource/appService/list)配置NetworkPolicy。在华为云上提供了vpc网络和容器隧道网络，仅“容器隧道网络”模式的集群支持网络隔离。
-   [资源配额限制](https://support.huaweicloud.com/usermanual-cce/cce_01_0287.html)，通过设置命名空间级别的资源配额，实现多租户在共享集群资源的情况下限制团队、租户可以使用的资源总量，包括限制命名空间下创建某一类型对象的数量以及对象消耗计算资源（CPU、内存）的总量，v1.9及以上版本的集群支持此功能。

    ![](figures/zh-cn_image_0000001322352285.png)

-   [RBAC权限管理策略](https://support.huaweicloud.com/usermanual-cce/cce_01_0189.html)，命名空间权限是基于Kubernetes RBAC能力的授权，通过权限设置可以让不同的用户或用户组拥有操作不同Kubernetes资源的权限，通过角色绑定，为不同角色或者租户定义访问Kubernetes资源（命名空间级别）的规则，如deployment、statefulSet、ConfigMap、Secret等。

    ![](figures/zh-cn_image_0000001271592428.png)


