# pod隔离模式<a name="ZH-CN_TOPIC_0000001271096436"></a>

在业务比较平稳的SaaS系统中，可以采用pod部署模式，不同租户的应用模块采用独立pod部署；[Pod](https://support.huaweicloud.com/basics-cce/kubernetes_0006.html)是Kubernetes创建或部署的最小单位，一个Pod封装一个或多个容器（container）、存储资源（volume）、一个独立的网络IP以及管理控制容器运行方式的策略选项，也可以用于租户业务资源的隔离，CCE集群支持CPU/内存配额限制、网络隔离、[QoS限速](https://support.huaweicloud.com/usermanual-cce/cce_01_0382.html)等策略。

-   Pod隔离依赖于流量染色和负载均衡，必须考虑Pod的高可用，Pod本身应该是无状态的，但租户请求应该有粘性，同一组租户的请求在同一个Pod内处理对缓存和连接池的使用是有优化的。
-   CPU/内存配额限制：指定部署不同租户应用的pod资源配置是避免CPU、内存资源相互抢占，对租户环境稳定性造成影响；在pod层级的资源配额限制可以在pod创建时，指定pod中容器被允许使用的最小资源和最大资源，也可以在滚动升级时配置。
-   网络隔离策略：在默认情况下，同一集群中的pod与pod之间可以相互访问，为保证部署不同租户应用的pod之间的隔离性，可以在NetworkPolicy中配置白名单，设置可以访问该pod资源的协议、目的容器端口、远端namespace或者工作负载。

    ![](figures/zh-cn_image_0000001271752392.png)

-   [QoS限速](https://support.huaweicloud.com/usermanual-cce/cce_01_0382.html)：由于不同租户资源可能部署在同一节点上，导致不同业务容器之间存在带宽抢占的情况，容易造成业务抖动。为了解决这个问题，您可以通过对Pod间互访进行QoS限速来解决这个问题。当然，也可以通过亲和性策略，将不同租户的pod调度到不同节点上，避免网络资源抢占。

