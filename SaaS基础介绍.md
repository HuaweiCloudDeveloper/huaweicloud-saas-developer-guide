# SaaS基础介绍<a name="ZH-CN_TOPIC_0000001271256348"></a>

当您想将企业的业务系统从现在的独立部署模式改造为SaaS模式，或者是全新开发一个SaaS业务系统，您可以参考由华为云开发者技术服务团队提供的SaaS化应用开发套件（如[开发指南](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saas-developer-guide)，[示例代码](https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper)、[SaaS开发插件](https://gitee.com/HuaweiCloudDeveloper/saas-tenant-router-starter)）。本文档会围绕企业在SaaS化转型过程中遇到的问题来展开梳理，在您阅读完本文档之后，如果对于SaaS化云原生应用构建，您还找不到答案，如客户案例、技术方案、可用的云服务和价格等，可以在社区中预约我们的工程师交流，请通过[gitee反馈](https://gitee.com/organizations/HuaweiCloudDeveloper/issues)或者提[开发者工单](https://support.developer.huaweicloud.com/feedback)，我们将及时给您答复。

SaaS作为一种有效的软件交付形式，可以让企业IT团队将工作的重心从部署和业务系统定制，转移到管理业务系统所提供的服务上来。SaaS应用交付便捷、节省成本，已经成为许多中小企业转型的首选，但SaaS开发过程中的各种问题，如人力成本、多租户管理、高可用、安全性等，让企业在SaaS化转型时有所顾虑。

