# 华为云SaaS支持计划<a name="ZH-CN_TOPIC_0000001321736293"></a>

当已经清晰的认识了[SaaS化转型准备](SaaS化转型面临的问题.md)中的问题后，要让产品功能、性能以及安全性等对业务有很好的满足度，需要经历一个比较长的时间，参与SaaS转型的团队成员必须做好准备；对企业在SaaS化转型中，可能遇到的痛点问题，华为云开发者技术服务团队为企业准备如下资源：

-   SaaS应用开发技术支持套件，包括[SaaS应用开发指南](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saas-developer-guide)  、[SaaS开发示例代码](https://gitee.com/HuaweiCloudDeveloper/saas-housekeeper)、[SaaS开发插件](https://gitee.com/HuaweiCloudDeveloper/saas-tenant-router-starter)以及配套的培训课程等。
    -   开发指南为SaaS应用开发者提供云原生SaaS架构、设计等参考。
    -   示例代码为开发者提供SaaS开发示例参考代码，使用SpringCloud微服务框架，同时提供了接入华为云微服务引擎CSE版本。
    -   SaaS开发中通用能力封装成插件，如SaaS多租路由插件，提供多租设计通用方案，简化SaaS改造时，数据多租路由开发。
    -   除了以上内容，对[数据分析](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-analytics-reference-architecture)、[日志处理](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-lts-sdk-intergration-sample)、[安全扫描](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-vss-api-jenkins-intergration-sample)等也提供了相关方案，以开放至[开源社区](https://gitee.com/HuaweiCloudDeveloper/)。


-   云资源支持，加入[华为云SaaS星光计划](https://www.huaweicloud.com/partners/saas/index.html)，申请华为云资源支持。
-   商业渠道支持，大量伙伴的SaaS应用上架至[华为云应用商店](https://marketplace.huaweicloud.com/)，实现商业变现，参考：[SaaS接入方式](https://support.huaweicloud.com/usermanual-marketplace/zh-cn_topic_0000001073925566.html)。
-   [在线问题咨询](https://support.developer.huaweicloud.com/feedback/)，可以进行解决方案、云上开发bug等问题咨询，提供SLA机制保证。

