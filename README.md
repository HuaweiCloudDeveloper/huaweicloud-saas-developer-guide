# README<a name="ZH-CN_TOPIC_0000001340290341"></a>

[文档首页](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-saas-developer-guide/blob/master-dev/zh-cn_bookmap_0000001321775789.md)

如果您对本文档有任何优化意见，都可以通过[gitee反馈](https://gitee.com/organizations/HuaweiCloudDeveloper/issues)，提交文档变更申请，我们会及时答复您的申请。

**修改文档**

1.  登录[gitee](https://gitee.com/HuaweiCloudDeveloper)网站。
2.  打开需要编辑的内容所在的页面。
3.  在页面的右侧，单击“编辑”。
4.  对文档进行修改后，单击“提交审核”

官方文档链接：[SaaS应用开发指南](https://support.developer.huaweicloud.com/doc/development/ApplicationScenarioDevelopmentInCloud-guides/zh-cn_topic_0000001271256348-0000001271256348)

安全能力要求框架：http://www.ctiforum.com/news/guonei/603767.html

